<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\PostController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

// USER
Route::get('/user', [UserController::class, 'index']);
Route::post('/signup', [UserController::class, 'store']);
Route::get('/user/{id}', [UserController::class, 'show']);
Route::get('/user/{role}', [UserController::class, 'getAllUser']);
Route::put('/user/{id}', [UserController::class, 'update']);
Route::delete('/user/{id}', [UserController::class, 'destroy']);
Route::post('/signin', [UserController::class, 'singin']);
Route::post('/signout', [UserController::class, 'signout']);




// CATEGORY
Route::get('category',[CategoryController::class,'getAllCategory']);
Route::post('category',[CategoryController::class,'createCategory']);
Route::get('category/{category_id}',[CategoryController::class,'show']);
Route::put('category/{category_id}',[CategoryController::class,'updateCategory']);
Route::delete('category/{category_id}',[CategoryController::class,'deleteCategory']);
Route::get('/category/search/{name}', [CategoryController::class, 'searchCategory']);



// POST
Route::get('post', [PostController::class, 'index']);
Route::post('post', [PostController::class, 'store']);
Route::get('post/{id}', [PostController::class, 'show']);
Route::put('post/{id}', [PostController::class, 'update']);
Route::delete('post/{id}', [PostController::class, 'destroy']);
Route::get('/post/search/{title}', [PostController::class, 'searchByTitle']);
Route::get('/post/search/{category_id}', [PostController::class, 'searchByCat']);
