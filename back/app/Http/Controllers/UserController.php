<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return User::latest()->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:30', 'unique:users'],
            'role' => ['required', 'string'],
            'password' => ['required', 'string', 'min:8'],
        ]);
        $request->file('profile')->store('public/UserProfile');

        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->role = $request->role;
        $user->password = bcrypt($request->password);
        $user->profile = $request->file('profile')->hashName();
        $user->save();


        // $token = $user->createToken('mytoken')->plainTextToken;
        return response()->json([
            'user' => $user,
            // 'token'=> $token
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return User::findOrFail($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:30', 'unique:users'],
            'role' => ['required', 'string'],
            'password' => ['required', 'string', 'min:8'],
        ]);
        $request->file('profile')->store('public/UserProfile');

        $user = User::findOrFail($id);
        $user->name = $request->name;
        $user->email = $request->email;
        $user->role = $request->role;
        $user->password = bcrypt($request->password);
        $user->profile = $request->file('profile')->hashName();
        $user->save();


        // $token = $user->createToken('mytoken')->plainTextToken;
        return response()->json([
            'user' => $user,
            // 'token'=> $token
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $isDelete = User::destroy($id);
        if ($isDelete) {
            return response()->json(["message" => "quited"], 201);
        }
        return response()->json(["message" => "quit_error"], 404);
    }

    /**
     * Sign out of the application and dlete token.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function signout(Request $request)
    {
        // auth()->user()->tokens()->delete();
        return response()->json(['message' => 'signout']);
    }


    public function singin(Request $request)
    {
        $email_err = "";
        $password_err = "";
        $status = 200;
        $user = User::where('email', $request->email)->first();
        if (!$user) {
            $email_err = "Wrong your email";
            $status = 401;
        } else if (!Hash::check($request->password, $user->password)) {
            $password_err = "Wrong your password!";
            $status = 401;
        }
        return response()->json([
            'user' => $this->index(),
            'email_err' => $email_err,
            'password_err' => $password_err
        ], $status);
    }


    public function getAllUser($role)
    {
        return User::where('role','like','%'.$role.'%')->get();
    }
}
