<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Post::latest()->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'description' => 'required',
            'category_id'=>'required',
            'image' =>'image|mimes:jpg,png,jpeg,gif|max:19999',
        ]);
        $request->file('image')->store('public/pictures');
        $post = new Post();
        $post->title = $request->title;
        $post->description = $request->description;
        $post->category_id = $request->category_id;
        $post->image = $request->file('image')->hashName();

        $post->save();
        return response()->json(['message'=> 'post Created', 'data'=> $post], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Post::findorfail($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'title' => 'required',
            'description' => 'required',
            'category_id'=>'required',
            'picture' =>'image|mimes:jpg,png,jpeg,gif|max:19999',
        ]);
        $request->file('image')->store('public/pictures');
        $post = Post::findorfail($id);
        $post->title = $request->title;
        $post->description = $request->description;
        $post->category_id = $request->category_id;
        $post->image = $request->file('image')->hashName();

        $post->save();
        return response()->json(['message'=> 'post Created', 'data'=> $post], 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $isDelete = Post::destroy($id);
        if ($isDelete) {
            return response()->json(["message" => "quited"], 201);
        }
        return response()->json(["message" => "quit_error"], 404);
    }
        /**
     * search post by title.
     *
     * @param  int  $title
     * @return \Illuminate\Http\Response
     */
    public function searchByTitle($title)
    {
        return Post::where('title','like','%'.$title.'%')->get();
    }
        /**
     * search post by id of category.
     *
     * @param  int  $title
     * @return \Illuminate\Http\Response
     */
    public function searchByCat($category_id)
    {
        return Post::where('category_id','like','%'.$category_id.'%')->get();
    }
}
