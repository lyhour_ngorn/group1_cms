import Vue from 'vue'
import App from './App.vue'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import router from "@/router/index.js"


// Make BootstrapVue available throughout your project
Vue.use(BootstrapVue)
// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin)
// import { fab } from '@fortawesome/free-brands-svg-icons'
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";
import { library } from "@fortawesome/fontawesome-svg-core";


import { 
  faHome, 
  faListAlt,
  faPlusCircle,
  faTasks,
  faTh,
  faPencilAlt,
  faCog,
  faUsers,
  faUser,
  faSignOutAlt,
  faEdit,
  faTrashAlt,
  faClock,
 } from "@fortawesome/free-solid-svg-icons";
library.add(faHome, faListAlt, faPlusCircle,faTasks,faTh,faCog,faUsers,faSignOutAlt,faUser,faEdit,faTrashAlt,faClock,faPencilAlt);


// library.add(fab)
Vue.component("font-awesome-icon", FontAwesomeIcon);


// Import Bootstrap an BootstrapVue CSS files (order is important)


// Make BootstrapVue available throughout your project
Vue.use(BootstrapVue)
// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin)

Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')


