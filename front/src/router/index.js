import Vue from "vue";
import VueRouter from 'vue-router';
// import { component } from "vue/types/umd";
import Login from '@/components/views/Login.vue';
import AllPost from '@/components/views/AllPost.vue';
import Navigator from '@/components/navigration/Nav.vue';
import ReadDetail from '@/components/views/ReadDetail.vue';
import UserProfile from '@/components/views/UserProfile.vue';
import Create_Post from '@/components/views/Create_Post.vue';
import Category from '@/components/views/Category.vue';
import ManageUser from '@/components/views/ManageUser.vue';
 Vue.use( VueRouter)

 const routes = [
     {
     path: "/",
     component:Login
 },
 {

       path: "/dashboard",
       component:Navigator
 },
 {

       path: "/allpost",
       component:AllPost
 },
 {

       path: "/readdetail",
       component:ReadDetail
},
{

       path: "/userprofile",
       component:UserProfile
},
{

       path: "/createpost",
       component:Create_Post
},
{

       path: "/category",
       component:Category
},

{

       path: "/manageuser",
       component:ManageUser
},

 ] 

 const router = new VueRouter({
     mode :'history',
     base: process.env.BASE_URL,
    routes,
 }) 
 export default router;